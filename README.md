Problem: self.pk is not 100% reliable to check if an instance of a model is new or not.

The documentation is not explicit enough in my opinion

https://docs.djangoproject.com/en/dev/ref/models/fields/#default

https://docs.djangoproject.com/en/dev/ref/models/fields/#uuidfield

https://docs.djangoproject.com/en/dev/ref/models/instances/#django.db.models.Model.from_db


```bash
git clone https://gitlab.com/luzfcb/self_pk_is_not_reliable.git
cd self_pk_is_not_reliable
mkvirtualenv -p python3.8 -a $(pwd) -r requirements.txt $(basename $(pwd))
pytest
```
