from django.apps import AppConfig


class CustomPrimaryKeyWithDefaultConfig(AppConfig):
    name = 'custom_primary_key_with_default'
