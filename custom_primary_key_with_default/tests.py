import copy

import pytest
import logging
from .models import PrimaryKeyHasDefaultModel, NormalModel, ModelInheritance, InitialState


@pytest.mark.parametrize(
    "model",
    [
        NormalModel,
        PrimaryKeyHasDefaultModel,
        ModelInheritance
    ]
)
@pytest.mark.django_db
def test_pk_of_model(caplog, model):
    caplog.set_level(logging.DEBUG)

    # create a new instance of the model
    new_instance = model()
    new_instance_is_new_variable_before_save = new_instance.is_new
    new_instance_state_before_save = new_instance._state
    new_instance_pk_before_save = new_instance.pk
    new_instance_adding_value_before_save = new_instance_state_before_save.adding

    new_instance.save()
    new_instance_is_new_variable_after_save = new_instance.is_new
    new_instance_logs = copy.copy(caplog.messages)
    caplog.clear()

    new_instance_pk_after_save = new_instance.pk
    new_instance_state_instance_after_save = new_instance._state
    new_instance_adding_value_after_save = new_instance_state_instance_after_save.adding

    _tmp_instance = model.objects.create()
    caplog.clear()
    existing_instance = model.objects.get(pk=_tmp_instance.pk)

    existing_instance_is_new_variable_before_save = existing_instance.is_new

    existing_instance_state_before_save = existing_instance._state
    existing_instance_pk_before_save = existing_instance.pk
    existing_instance_adding_value_before_save = existing_instance_state_before_save.adding

    existing_instance.save()
    existing_instance_is_new_variable_after_save = existing_instance.is_new
    existing_instance_logs = copy.copy(caplog.messages)

    existing_instance_pk_after_save = existing_instance.pk
    existing_instance_state_after_save = existing_instance._state
    existing_instance_adding_value_after_save = existing_instance_state_after_save.adding

    assert existing_instance_is_new_variable_before_save is InitialState
    assert existing_instance_is_new_variable_after_save is not InitialState
    assert existing_instance_is_new_variable_after_save is False
    assert 'self.is_new is True. Running only once after saving' not in existing_instance_logs
    assert existing_instance_state_before_save is existing_instance_state_after_save, "The model._state object changed after save"
    assert existing_instance_adding_value_after_save is False, "The model_instance._state.adding should be False after save"
    assert existing_instance_adding_value_before_save is False, "The model_instance._state.adding should be False before save"
    assert existing_instance_pk_after_save is not None, "The model_instance.pk should be not None after save"
    assert existing_instance_pk_before_save is not None, "The model_instance.pk should be not None before save"

    assert new_instance_is_new_variable_before_save is InitialState
    assert new_instance_is_new_variable_after_save is not InitialState
    assert new_instance_is_new_variable_after_save is True
    assert 'self.is_new is True. Running only once after saving' in new_instance_logs
    assert new_instance_state_before_save is new_instance_state_instance_after_save, "The model_instance._state object changed after save"
    assert new_instance_adding_value_after_save is False, "The model_instance._state.adding should be False after save"
    assert new_instance_adding_value_before_save is True, "The model_instance._state.adding should be True before save"
    assert new_instance_pk_after_save is not None, "The model_instance.pk should be not None after save"
    assert new_instance_pk_before_save is None, "The model_instance.pk should be None before save"


