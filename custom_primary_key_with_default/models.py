import random
import uuid
from django.db import models
import logging

LOGGER = logging.getLogger("custom_primary_key_with_default_logger")


def random_number_as_str():
    """
    stupid insecure primary key id generator
    :return: str
    """
    return str(random.randrange(1, 10000))


class _InitialState:
    pass


InitialState = _InitialState()


class BaseModel(models.Model):
    class Meta:
        abstract = True

    def __str__(self):
        return str(self.pk)

    is_new = InitialState

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Using self._state.adding is a workaround to check the creation state.
        # This is required because self.pk has a non-null value when the model class
        # uses a primary key with the argument "default" has a value or callable

        # The "is_new" object is a way to know if this models instance come from
        # a new instance or come from the database, because self.pk is not 100% reliable for that purpose.
        # This can be useful if you do not want or cannot use signals and
        # want to implement a post_save logic that only and exclusively executes if a model instance is new.
        self.is_new = self._state.adding

        super().save(force_insert, force_update, using, update_fields)
        if self.is_new:
            msg = "self.is_new is True. Running only once after saving"
            LOGGER.debug(msg)


class NormalModel(BaseModel):
    pass


class PrimaryKeyHasDefaultModel(BaseModel):

    my_primary_key = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    #  although I identified this behavior using UUIDField, the behavior is
    #  the same when we have any primary key field with the default argument filled

    # when we have Field(primary_key=True, default=callable|value)
    # id = models.CharField(
    #     max_length=6,
    #     unique=True,
    #     primary_key=True,
    #     default=random_number_as_str,
    # )


class ModelInheritance(PrimaryKeyHasDefaultModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
